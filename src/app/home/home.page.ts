import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

  generateError() {
    throw new Error('I am a bug 0.0.2 ... 🐛');
  }

}
