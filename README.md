# Ionic 4 with sentry integration

----
## Project setup

### 1. create account on sentry,

### 2. create cordova project

### 3. create ionic project

```ionic start ionicSentry blank --cordova --type=angular --package-id=com.ionic.sentry```

### 5. add sentry-cordova npm package

```npm i --save sentry-cordova```

### 6. add cordova platform
```ionic cordova platform add ios```

### 7. add sentry-cordova plugin
```cordova plugin add sentry-cordova```

### 8. setup package.json to generate source map

```
//package.json
"config": {
    "ionic_generate_source_map": "true"
}
```

### 9. extend sentry error handling from angular error

```
//app.module.ts
import * as Sentry from 'sentry-cordova';
Sentry.init({ dsn: 'https://<key>@sentry.io/<project>' });
export class SentryIonicErrorHandler extends ErrorHandler {
  handleError(error) {
    super.handleError(error);
    try {
      Sentry.captureException(error.originalError || error);
    } catch (e) {
      console.error(e);
    }
  }
}
```

```
//app.module.ts
    {provide: ErrorHandler, useClass: SentryIonicErrorHandler}
```

### 10. create error event

```
// home.page.ts
  generateError() {
    throw new Error('I am a bug 0.0.1 ... 🐛');
  }
```

```
// home.page.html
    <ion-button (click)="generateError()" >Generate Error</ion-button>
```

### 11. run ionic project to device
```
ionic cordova run ios --prod --source-map
```

### 12. upload source map to sentry

```
sentry-cli releases -o psycho-dev -p ionic-sentry files com.ionic.sentry-0.0.1 upload-sourcemaps platforms/ios/build/device/IonicSentry.app/www --rewrite
```

----
## Ionic build will automatically upload the source map

```
export SENTRY_RELEASE_STRING=com.ionic.sentry-0.0.2
```

```
ionic cordova build ios --prod --source-map
```

```
ios-deploy -b /Users/atmosuwiryo/gawean/ionic/4.3.1/ionicSentry/platforms/ios/build/device/IonicSentry.ipa
```

----
## Disable ionic build auto upload source map
```
export SENTRY_SKIP_AUTO_RELEASE=true
```

----
## Reference
[Sentry Docs](https://docs.sentry.io/platforms/javascript/ionic/)
